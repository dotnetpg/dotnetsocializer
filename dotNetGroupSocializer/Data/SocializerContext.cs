﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace dotNetGroupSocializer.Models
{
    public class SocializerContext : DbContext
    {
        public SocializerContext (DbContextOptions<SocializerContext> options)
            : base(options)
        {
        }

        public DbSet<dotNetGroupSocializer.Models.Product> Product { get; set; }
    }
}
