﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using dotNetGroupSocializer.Models;

namespace dotNetGroupSocializer.Pages.Products
{
    public class IndexModel : PageModel
    {
        private readonly dotNetGroupSocializer.Models.SocializerContext _context;

        public IndexModel(dotNetGroupSocializer.Models.SocializerContext context)
        {
            _context = context;
        }

        public IList<Product> Product { get;set; }

        public async Task OnGetAsync()
        {
            Product = await _context.Product.ToListAsync();
        }
    }
}
